package main

import (
	"database/sql"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	_ "github.com/lib/pq"
	"log"
	"net/http"
)

type FamilyMember struct {
	ID       int    `json:"id"`
	Name     string `json:"name"`
	Gender   string `json:"gender"`
	ParentID *int   `json:"parent_id"`
}

var db *sql.DB

func main() {
	var err error

	// Connect to the PostgreSQL database
	db, err = sql.Open("postgres", "postgres://pguser:pgpass@localhost/family_tree?sslmode=disable")
	if err != nil {
		log.Fatal("Failed to connect to the database:", err)
	}
	defer db.Close()

	// Create the family members table if it doesn't exist
	_, err = db.Exec(`CREATE TABLE IF NOT EXISTS family_members (
		id SERIAL PRIMARY KEY,
		name TEXT,
		gender TEXT,
		parent_id INTEGER
	)`)
	if err != nil {
		log.Fatal("Failed to create the family_members table:", err)
	}

	// Initialize the Gin router
	r := gin.Default()

	config := cors.DefaultConfig()
	config.AllowOrigins = []string{"http://127.0.0.1:3000"}  // Replace with the URL of your frontend application
	config.AllowMethods = []string{"GET", "POST", "OPTIONS"} // Add any additional HTTP methods you need to support
	config.AllowHeaders = []string{"Origin", "Content-Type"}
	r.Use(cors.New(config))
	// Define the API routes
	r.GET("/", getRootResponse)
	r.GET("/family-members", getFamilyMembers)
	r.POST("/family-members", createFamilyMember)

	// Start the server
	log.Println("Server listening on port 8000")
	log.Fatal(r.Run("127.0.0.1:8000"))
}

func getRootResponse(c *gin.Context) {
	var simpleResponse = "OOPS! No way here..."
	c.JSON(http.StatusOK, simpleResponse)
}

func getFamilyMembers(c *gin.Context) {
	rows, err := db.Query("SELECT id, name, gender, parent_id FROM family_members")
	if err != nil {
		log.Println("Failed to fetch family members:", err)
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to fetch family members"})
		return
	}
	defer rows.Close()

	var familyMembers []FamilyMember

	for rows.Next() {
		var member FamilyMember
		var parentID sql.NullInt64
		err := rows.Scan(&member.ID, &member.Name, &member.Gender, &parentID)
		if err != nil {
			log.Println("Error scanning family member:", err)
			continue
		}
		if parentID.Valid {
			parentIDValue := int(parentID.Int64)
			member.ParentID = &parentIDValue
		}
		familyMembers = append(familyMembers, member)
	}

	if err := rows.Err(); err != nil {
		log.Println("Error retrieving family members:", err)
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to retrieve family members"})
		return
	}

	c.JSON(http.StatusOK, familyMembers)
}

func createFamilyMember(c *gin.Context) {
	var member FamilyMember
	if err := c.ShouldBindJSON(&member); err != nil {
		log.Println("Failed to decode request body:", err)
		c.JSON(http.StatusBadRequest, gin.H{"error": "Failed to decode request body"})
		return
	}

	var parentIDValue interface{}
	if member.ParentID != nil {
		parentIDValue = *member.ParentID
	} else {
		parentIDValue = nil
	}

	stmt, err := db.Prepare("INSERT INTO family_members(name, gender, parent_id) VALUES($1, $2, $3)")
	if err != nil {
		log.Println("Failed to prepare insert statement:", err)
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to create family member"})
		return
	}
	defer stmt.Close()

	_, err = stmt.Exec(member.Name, member.Gender, parentIDValue)
	if err != nil {
		log.Println("Failed to insert family member:", err)
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to create family member"})
		return
	}

	c.Status(http.StatusCreated)
}
